//
//  ViewController.swift
//  MapApplication
//
//  Created by IDS Comercial on 22/02/18.
//  Copyright © 2018 IDS Comercial. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate{
    @IBOutlet weak var mapView: MKMapView! //Se necesita importar el mapKit Framework En Capabillities
    
    var latitud = 51.501364
    var longitud = -0.1418899999999894
    
    var manager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let span = MKCoordinateSpanMake(0.01, 0.01)//Este es como el zoom para cuando inicie
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitud, longitude: longitud), span: span)//Lo q va adentro de los parentesis y de color son las variables antes declaradas
       
        mapView.setRegion(region, animated: true)//Todo esto seria para se ubique desde q inicia la app
        
        let pinLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitud, longitud) //
        let pinObject = MKPointAnnotation()
        pinObject.coordinate = pinLocation
        pinObject.title = "Buckingham Palace"
        pinObject.subtitle = "Westminster, London SW1A 1AA, UK"
        
        self.mapView.addAnnotation(pinObject)//Coloca un pin en el mapa con las caracteristicas de arriba
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func standard(_ sender: Any) { //Funcion para que sea vista estandar, despues del . son propiedades
        mapView.mapType = MKMapType.standard
    }
    
    @IBAction func satellite(_ sender: Any) {
        mapView.mapType = MKMapType.satellite
    }
    
    @IBAction func Hybrid(_ sender: Any) {
        mapView.mapType = MKMapType.hybrid
    }
    
    @IBAction func locateme(_ sender: Any) {//colocas lo privaci en el info.plist
        manager.delegate = self //Se tiene q declarar el delegate arriba
        
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        mapView.showsUserLocation = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {//Te localizara  donde estes
        let userlocation:CLLocation = locations[0] as CLLocation
        manager.stopUpdatingLocation()
        
        let location = CLLocationCoordinate2D(latitude: userlocation.coordinate.latitude, longitude: userlocation.coordinate.longitude)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func directions(_ sender: Any) {//Para q de la direccion y te mande ahi la ruta
        
        UIApplication.shared.open(URL(string: "http://maps.apple.com/maps?daddr=\(latitud),\(longitud)")!, options: [:], completionHandler: nil)
    }
}

